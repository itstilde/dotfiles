#!/bin/bash
#http://www.howtoforge.com/how-to-benchmark-your-system-cpu-file-io-mysql-with-sysbench

    


echo "gime da mysql root pw please"
ROOT_MYSQL_PW=''
read ROOT_MYSQL_PW

[[ $ROOT_MYSQL_PW == "" ]] && echo "No password given" && exit 255
echo "Preparing benchmark, running:"
echo "sysbench --test=oltp --oltp-table-size=1000000 --mysql-db=test --mysql-user=root --mysql-password=${ROOT_MYSQL_PW} prepare "
sysbench --test=oltp --oltp-table-size=1000000 --mysql-db=test --mysql-user=root --mysql-password=${ROOT_MYSQL_PW} prepare 


#confirm 
YNQ_RET=0
YNQ="MAYBE"
while [ "${YNQ}" != "YES" ] && [ "${YNQ}" != "NO" ] && [ "${YNQ}" != "Y" ] && [ "${YNQ}" != "N" ]; do
echo -n "Confirm benchmark pls by typing"
echo -n " \"yes\" or \"no\" or \"q\" to quit " >&2
read YNQ
YNQ=`echo ${YNQ} | tr '[:lower:]' '[:upper:]'`
if [ "${YNQ}" = "Q" ] || \
    [ "${YNQ}" = "QUIT" ]; then
    echo "USER ABORT"
    exit 255
    
fi
done

if [ "${YNQ}" = "YES" ] || [ "${YNQ}" = "Y" ]; then
	YNQ_RET=1
else
	YNQ_RET=2
fi

if [[ $YNQ_RET == 1 ]]; then  

	#LOGFILE=/root/benchmark_logs/$(date -u "+%Y-%m-%d")-benchmark.txt
	#[[ ! -f $LOGFILE ]] && touch $LOGFILE
	#echo $LOGFILE;
	echo "===================================================="
	echo "#                BENCHMARK GO                      #"
	echo "===================================================="
	echo ""
	#echo " saving to ${logfile}"
	#echo "sysbench --test=oltp --oltp-table-size=1000000 --mysql-db=test --mysql-user=root --mysql-password=${ROOT_MYSQL_PW} --max-time=60 --oltp-read-only=on --max-requests=0 --num-threads=8 run 2>&1 > tee ${$LOGFILE}"
	sysbench --test=oltp --oltp-table-size=1000000 --mysql-db=test --mysql-user=root --mysql-password=${ROOT_MYSQL_PW} --max-time=60 --oltp-read-only=on --max-requests=0 --num-threads=8 run 
	echo "===================================================="
	echo "#               BENCHMARK DONE                     #"
	echo "===================================================="

	echo "Removing previous benchmarks"
	#echo "sysbench --test=oltp --mysql-db=test --mysql-user=root --mysql-password=${ROOT_MYSQL_PW} cleanup "
	sysbench --test=oltp --mysql-db=test --mysql-user=root --mysql-password=${ROOT_MYSQL_PW} cleanup 
	echo "... Done cleaning up"

fi

