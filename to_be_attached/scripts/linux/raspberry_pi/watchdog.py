#!/usr/bin/python2
 
# this script is supposed to connect to xbmc on port 9090 and wait for incoming event-notifications
# on the onPlay-Event it changes the xbmc-process-priority to 10 and its scheduler to SHED_RR. It also changes the cpu-governor to performance.
# on the onStop-Event it reverts the process-priority to its default state and changes the cpu-governor to ondemand.
#
# coded by CruX (crux@project-insanity.org)
 
import socket
import time
import subprocess
 
def countOpen (input):
        return input.count('{')
 
def countClose (input):
        return input.count('}')
 
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
elapsed = 0
 
def loop():
        pid = int(subprocess.check_output(["/bin/pidof", "xbmc.bin"]))
        print pid
        while True:
                opening = 0
                closing = 0
                s.setblocking(1)
                data = ""
                tmp = s.recv(8)
                if (tmp == ""):
                        break
                opening += countOpen(tmp)
                closing += countClose(tmp)
                data += tmp
 
                s.setblocking(0)
                while True:
                        tmp = bytearray(128)
                        count = s.recv_into(tmp, 128)
                        opening += countOpen(tmp[:count])
                        closing += countClose(tmp[:count])
                        data += tmp[:count].decode("utf-8")
 
                        if opening == closing:
                                break
 
                        time.sleep(.1)
 
                if (data.count("\"type\":\"movie\"") > 0 or data.count("\"type\":\"episode\"") > 0):
                        if (data.count("\"method\":\"Player.OnPlay\"") > 0):
                                subprocess.check_output(["/usr/bin/chrt", "-a", "-p", "10", str(pid)])
                                gov = open("/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor", "w")
                                gov.write("performance")
                                gov.close()
                        elif (data.count("\"method\":\"Player.OnStop\"") > 0):
                                subprocess.check_output(["/usr/bin/chrt", "-a", "-o", "-p", "0", str(pid)])
                                gov = open("/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor", "w")
                                gov.write("ondemand")
                                gov.close()
        s.close()
 
while elapsed < 120:
        try:
                s.connect(("127.0.0.1", 9090))
                loop()
                elapsed = 0
        except socket.error:
                time.sleep(1)
                elapsed += 1
