#!/usr/bin/env bash
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#                          
#                           Greetings :3                                      #
#        )(  ) )         _                               _  _______  _        #
#       (  ) (          / \                      　    ／   　　　　   ＼　   #
#        )(  )         /   \                    　    /___∠__________＼_|     #
#         ) (         /     \                  　      | |　 ＿ ||  | | 　    # 
#        =====       /       \                         | |∠二ﾆ二||> | |　　　 #　                      
#        |   |      /         \                       /　 _　　 ||  _  \　 　 # 
#  ______|___|_____/           \_______________ 　   /  ( ・）  ||（・) \　   #　 
# |:::::::::::::::/             \::::::::::::::|　  二土     　c||　　 土二   # 
# |::::::::::::::/               \:::::::::::::|   二=   ___    ||っ___ = 二  #
# |:::::::::::::/                 \::::::::::::|    / ﾉ_____／ ξﾉ_____,   \　 #　 　 
# =============/===================\============ 　/  /　 へ 　へＪ1   \   \  #
#  |#|                                      |#|   /  /　へ　へ｀-'._へ  |  |  #
#  |#|  AUTHOR :  Jacob K.                  |#|   |  ||  へ 　へ  へ　へ＼ |  # 
#  |#|   EMAIL :  jacob@the.nekoba.su       |#|   |  |　　　 　　　　　  | |  # 
#  |#|     WEB :  www.nekoba.su             |#|   |  |　　　 　　　      | |  # 
#  |#| TWITTER :  @killgallic               |#|   |  |　　　 　　　      | |  # 
#  |#|                                      |#|   ヽ_∥ 　   　　　　  　 | |  # 
#  |#|                                      |#|   | |　　　　　　　  　  | |  #
#  |#|                                      |#|   l　ヽ　 　　　　　  　/  /  # 
#  |#| LICENSE :  Private                   |#|    ヽ ＼　 　　　      /  /   #          
#  |#|            Unless otherwise stated   |#|     ＼  ＼＿＿＿＿＿＿／ /    #
#  |#|======================================|#|   　  ＼ ______^_______ /     #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

#=============================================================================#
#
# FILE:  functions
#
# PURPOSE: functions are stored here to keep ~/.bashrc a bit clean
#	 
#==============================================================================# 
#==============================================================================#

# Quick logging of RAM

function memoryLog {
        logfile="/tmp/MEM.log"
        echo "Started logging memory usage at at $(date)" > $logfile
        while :
            do
                echo -e $(date "+%H:%M:%S") 2>&1 | tee -a $logfile
                echo -e "$(free -m)" 2>&1 | tee -a $logfile
                echo -e "-----------------------------------------------------------\n" >> $logfile
                sleep 1
        done
}
# Quick dirty logging of CPU
function CPULog {
        logfile="/tmp/CPU.log"
        echo "Started logging CPU usage at $(date)" > $logfile
        while :
            do
                echo -e $(date "+%H:%M:%S") 2>&1 | tee -a $logfile
                echo -e "$(sar -P ALL 1 1)" 2>&1 | tee -a $logfile
                echo -e "-----------------------------------------------------------\n" >> $logfile
                sleep 1
        done
}
# Logs the top 5 processes from the top command
function topLog { 
    while :; do echo $(date "+%H:%M:%S") 2>&1 | tee -a /tmp/top.log;  top -b -n 1 | grep -A 5 "PID USER" 2>&1 | tee -a /tmp/top.log; sleep 3; done
}
function mysqlTop {
    echo -e "Provide mysql user details \n"
    echo "user:"
    read user
    echo "password"
    read password
    watch "echo 'show processlist' | mysql -u $user --password=$password | grep -v \"show processlist\" "
}
function waitssh {
    if [[ -z "$1" ]]; then
        echo " ${0##*/} Pass an argument u idiot"
    else 
        while(true)
        do
            ssh "$1"
            sleep 5
        done
    fi

}
# Easy Extractor
function extract {
    if [[ -z "$1" ]]; then
        echo " ${0##*/} <archive> - extract common file formats)"
    else 
        if [ -f $1 ] ; then
            case $1 in
                *.7z      ) 7z  x       "$1" ;;
                *.tar.bz2 ) tar xvjf    "$1" ;;
                *.bz2     ) bunzip2     "$1" ;;
                *.deb     ) ar  vx      "$1" ;;
                *.tar.gz  ) tar xvf     "$1" ;;
                *.gz      ) gunzip      "$1" ;;
                *.tar     ) tar xvf     "$1" ;;
                *.tbz2    ) tar xvjf    "$1" ;;
                *.tar.xz  ) tar xvf     "$1" ;;
                *.tgz     ) tar xvzf    "$1" ;;
                *.rar     ) unrar x     "$1" ;;
                *.zip     ) unzip       "$1" ;;
                *.Z       ) uncompress  "$1" ;;
                *         ) echo " Unsupported file format!" ;; 
            esac
        else
            echo "'$1' is not a valid file"
        fi
    fi
}

##SSH Reagent tychoish.com/rhizome/9-awesome-ssh-tricks
function sshreagent {
    for agent in /tmp/ssh-*/agent.*; do
	export SSH_AUTH_SOCK=$agent
	if ssh-add -l 2>&1 > /dev/null; then
	    echo Found working SSH Agent:
	    ssh-add -l
	    return
	fi
    done
    echo Cannot find ssh agent - maybe you should reconnect and forward it?
}


#uptimee for more friendly display
function uptime {
 read secs x </proc/uptime;
 printf "%s days, %s hours, %s minutes and %s seconds" $((${secs%.*}/3600/24)) $((${secs%.*}/3600%24)) $(($((${secs%.*}%3600))/60)) $(($((${secs%.*}%3600))%60)); 
}

# Lists the difference / growth rate of a file 
function file_size_change {
  if [ -z "$1" ]; then 
      perl -e '
      $file = shift; die "no file [$file]" unless -f $file; 
      $sleep = shift; $sleep = 1 unless $sleep =~ /^[0-9]+$/;
      $format = "%0.2f %0.2f\n";
      while(1){
	$size = ((stat($file))[7]);
	$change = $size - $lastsize;
	printf $format, $size/1024/1024, $change/1024/1024/$sleep;
	sleep $sleep;
	$lastsize = $size;
      }' "$1" "$2"
  else 
      echo 'Usage: fileSizeChange <file> <time|optional>';
  fi
}

#
# Timestamp a file.
#
function timestamp {
    # TODO add -m to use mv instead of cp
    local date_string="+%Y-%m-%d"

    if [ ! -z "$1" ]; then 
        if [[ "$1" -eq "me" ]]; then
            echo "$(date +%Y-%m-%d)"
        else 
            cp "$1" "$(date +%Y-%m-%d)-$1";
        fi
    else
        echo "$(date ${date_string})"
        echo "Please pass a file name to timestamp! e.g. timestamp backup.sql"
    fi 
}

# Some conditions that make life easier example below
function empty_string {
    local var="$1"

    [[ -z "$var" ]]
}

function non_empty_string {
    local var="$1"

    [[ -n "$var" ]]
}


function is_file {
    local file="$1"

    [[ -f "$file" ]]
}


function not_file {
    local file="$1"

    [[ ! -f "$file" ]]
}
function is_symlink {
    local dir="$1"

    [[ -L "$dir" ]]
}
function is_dir {
    local dir="$1"

    [[ -d "$dir" ]]
}

function copyit {
    # TODO Figure out how to copy command output e.g. df -mh | copyit
    # test if file or text? 
    local content	 # Content we're going to copy, be it contents of a file or a passed argument
    local is_file=false
    local copy_history="${HOME}/.clipboard_history"
    local file # for history, rather than sticking ALL The content in there just write what file we copied
    #sanity check for history file 
    if ! is_file "$copy_history" ; then
	echo 'Made copy history file'
	touch $copy_history
    fi

    # support files and strings / piped data 
    # Do actual copy now
    if  is_file "$1" ; then 
	#echo "yup a file"
	is_file=true
	file="$1" 
	# grab the file path so we can put that in history instead of file contents
	local file_path=$(readlink -e "${file}")	
	content=`cat "$1"` 
    elif is_not_empty "$1" && ! is_dir "$1" ; then 
	content="$@"  # $@ to grab aaaal of the contents
	#echo "isn't empty or a dir" 
    else 
	echo "give me something to copy shitlord" 
    fi

    # Add file/content to clipboard history if not already the most recent
    local lastline=$(tail -2 "${copy_history}" | head -1)
    if [[ "$is_file" == "true" ]] ; then 
	[[ "${lastline}" != "${file}" ]] && echo "File: ${file_path}" >> "${copy_history}" || echo "Already tried to copy it dude"
	
    else 
	[[ "${lastline}" != "${content}" ]] && echo "${content}" >> "${copy_history}" || echo "Already tried to copy it dude"

    fi

    # copy to clipboard depends on the OS
     case "$OSTYPE" in
	"cygwin" ) 
	    if [[ "$is_file" == "true" ]] ; then 

            echo `cat "$1"` > /dev/clipboard
            echo "copied file ${file_path} to clipboard"

	    else
            echo "${content}" > /dev/clipboard
            echo 'copied to clipboard' 

	    fi
	    ;;
	"darwin12" | "darwin13" )
	    # TODO Add copy function here
	;;
	"linux-gnu" ) 
	    # TODO Add copy function here
	;;
    esac 
}

# uploads an img to imgur
function imgur_upload {
    local imgur_key="486690f872c678126a2c09a9e196ce1b"
    if [ ! -z "$1" ]; then 
	local img_name=$(curl -s -F "image=@$1" -F "key=${imgur_key}" http://imgur.com/api/upload.xml | grep -E -o "<original_image>(.)*</original_image>" | grep -E -o "http://i.imgur.com/[^<]*" )
	copyit "${img_name}" > /dev/null 2>&1
	echo "${img_name}"
    else 
	echo "Please specify a file path" 
	echo " e.g. imgur_upload /path/to/image.jpg"
    fi
}

function get_stack {
   STACK=""
   # to avoid noise we start with 1 to skip get_stack caller
   local i
   local stack_size=${#FUNCNAME[1]}
   for (( i=1; i<$stack_size ; i++ )); do
      local func="${FUNCNAME[$i]}"
      [ x$func = x ] && func=MAIN
      local linen="${BASH_LINENO[(( i - 1 ))]}"
      local src="${BASH_SOURCE[$i]}"
      [ x"$src" = x ] && src=non_file_source

      STACK+=$'\n'"   "$func" "$src" "$linen
   done
}

function yesnoquit {
    # simple way to prompt the user for "yes, no, quit"
    # function will return either 1 for Yes or 2 for No
    # exits with code 255 for quit
    local YNQ_RET=0
    local YNQ="MAYBE"
    while [ "${YNQ}" != "YES" ] && [ "${YNQ}" != "NO" ] && [ "${YNQ}" != "Y" ] && [ "${YNQ}" != "N" ]; do
        echo -n "Please respond \"yes\" or \"no\" or \"q\" to quit: " >&2
        read YNQ
        YNQ=`echo ${YNQ} | tr '[:lower:]' '[:upper:]'`
        if [ "${YNQ}" = "Q" ] || \
            [ "${YNQ}" = "QUIT" ]; then
            echo "USER ABORT"
            return 255
        fi
    done
    if [ "${YNQ}" = "YES" ] || [ "${YNQ}" = "Y" ]; then
        YNQ_RET=1
    else
        YNQ_RET=2
    fi
    return ${YNQ_RET}
}

function apache_errortail() {
    DAVARLOGS=`find /var/log/httpd -regex '.*\.log$' -type f 2>/dev/null | grep err`
    DAWEBLOGS=`find /web/log -regex '.*\.log$' -type f 2>/dev/null | grep err`
    NOOP=0
    if [ "$1" != "" ] && [ ! -f $1 ]; then
        if [ "$1" == "-h" ] || [ "$1" == "--help" ] || [ "$1" == "-help" ] || [ "$1" == "-?" ]; then
            HEL="`basename $0` will find all WWW logs that it can and tail them. Optionally it will filter the log names by a given string and follow additionally specified log file not in normal WWW log paths."
            HEL="`echo -en \"${HEL}\" | fmt`\
  USAGE:\
    `basename $0` [filter] [extra file] [extra file] [extra file]... \
  DEFINITIONS: \
      [filter]      A string to filter log names found in normal WWW log paths\
                    by. For example \"bryn\" would match "bryn_err.log" but not\
                    \"php_err.log\".
      [extra file]  Additional files to \`tail\`. This can be any arbitrary file\
                    that exists."
            NOOP=1
        fi
        THELOGS=`echo -e "${DAVARLOGS}\n${DAWEBLOGS}" | grep -E "\\/log\\/.*($1|php_err)" | tr '\n' ' '`
    else
        THELOGS=`echo -e "${DAVARLOGS}\n${DAWEBLOGS}" | tr '\n' ' '`
    fi
    if [ ${NOOP} -eq 0 ]; then
        if [ "$2" != "" ] || [ -f $1 ]; then
            # extra logs
            if [ ! -f $1 ]; then
                EXTRAS="`echo $* | sed -e \"s@$1@@\"`"
            else
                EXTRAS="$*"
            fi
        fi
        echo "#"
        echo "# Tailing logs: ${THELOGS} ${EXTRAS}"
        echo "#"
        tail -f ${THELOGS} ${EXTRAS}
    fi
}

function php_lint() {
    for arg in $(seq $#)
    do  
        # Use find to iterate over directories recursively
        if [ -d "${!arg}" ]; then
            find "${!arg}" -name '*.php' -exec php -l "{}" ";"
        # Just run normal files and symlinks through php -l individually
        elif [ -f "${!arg}" -o -h "${!arg}" ]; then
            php -l "${!arg}"
        # Skip over anything else
        else
            echo 'Skipping bogus argument' 1>&2
        fi
    done
}


wiktionary() {
    curl -s "http://en.wiktionary.org/w/index.php?action=raw&title=$(echo $@ | sed 's/ /+/g')"
}


def() {
    wiktionary $@ | grep "^# " | perl -pe 's/\[\[(?:[^\|\]]+\|)*([^\]]+)\]\]/$1/g' | perl -pe 's/\{\{([^{|]+ of)\|([^|}]+)[^}]*\}\}/*\U$1\E* $2/g' | perl -pe 's/\{\{([^|}]+)[^}]*\}\}/\U*$1*\E/g' | sed -E 's/]]//'
}

def1() {
    def $@ | head -n 1
}

function rm_host() {
    sed -i "$1d" ~/.ssh/known_hosts
} 

excuse() {
    curl http://pages.cs.wisc.edu/~ballard/bofh/bofhserver.pl|grep "font size"|awk -F'>' '{print $8,$10}'|sed 's/<//'|sed 's/br//'
}
