#!/usr/bin/env bash
# Just misc shit for arch installs
# TODO figure out a way for these to auto execute so
# I don't  have to open a terminal for them to take effect

#xset r rate 290 25  # mouse rate w/ razor deathadder
#setxkbmap -option caps:none # disable caps

# removes orphaned packages from pacman
remove_orphans() {
  if [[ ! -n $(pacman -Qdt) ]]; then
    echo "No orphans to remove."
  else
    sudo pacman -Rns $(pacman -Qdtq)
  fi
}
