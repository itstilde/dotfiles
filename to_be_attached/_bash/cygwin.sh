#!/usr/bin/env bash
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#                          
#                                                                              # 
#                            Greetings :3                                      #
#         )(  ) )         _                               _  _______  _        #
#        (  ) (          / \                      　    ／   　　　　   ＼　   #
#         )(  )         /   \                    　    /___∠__________＼_|     #
#          ) (         /     \                  　      | |　 ＿ ||  | | 　    # 
#         =====       /       \                         | |∠二ﾆ二||> | |　　　 #　                      
#         |   |      /         \                       /　 _　　 ||  _  \　 　 # 
#   ______|___|_____/           \_______________ 　   /  ( ・）  ||（・) \　   #　 
#  |:::::::::::::::/             \::::::::::::::|　  二土     　c||　　 土二   # 
#  |::::::::::::::/               \:::::::::::::|   二=   ___    ||っ___ = 二  #
#  |:::::::::::::/                 \::::::::::::|    / ﾉ_____／ ξﾉ_____,   \　 #　 　 
#  =============/===================\============ 　/  /　 へ 　へＪ1   \   \  #
#   |#|                                      |#|   /  /　へ　へ｀-'._へ  |  |  #
#   |#|  AUTHOR :  Jacob K.                  |#|   |  ||  へ 　へ  へ　へ＼ |  # 
#   |#|   EMAIL :  jacob@the.nekoba.su       |#|   |  |　　　 　　　　　  | |  # 
#   |#|     WEB :  www.nekoba.su             |#|   |  |　　　 　　　      | |  # 
#   |#| TWITTER :  @killgallic               |#|   |  |　　　 　　　      | |  # 
#   |#|                                      |#|   ヽ_∥ 　   　　　　  　 | |  # 
#   |#|                                      |#|   | |　　　　　　　  　  | |  #
#   |#|                                      |#|   l　ヽ　 　　　　　  　/  /  # 
#   |#| LICENSE :  Private                   |#|    ヽ ＼　 　　　      /  /   #          
#   |#|            Unless otherwise stated   |#|     ＼  ＼＿＿＿＿＿＿／ /    #
#   |#|======================================|#|   　  ＼ ______^_______ /     # 
#                                                                              #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~# 
#
# FILE: 
#
# PURPOSE:
# 
# FLAGS:
#     -v  verbose mode 
#     -d  debugging mode 
#
# ARGUMENTS: 
# 
# EXAMPLES: 
#
#
#----------------------------------DESCRIPTION---------------------------------#
#    
#
#
#
#==============================================================================# 
# check heler file is active Helper file :3 
if [ -f "$HOME/.bash/helper.sh" ]; then
    source $HOME/.bash/helper.sh
else
     perl -e 'print("#", "-" x 78, "#\n");'
     perl -e 'print("#", " " x 28, "No helper script found", " " x 28, "#\n");'
     source ${HOME}/.bash/global_misc.sh && failboat
     exit 2;
fi 

#===============================================================================#
#                                                                                # 
#                   Yo Program starts under here silly billy                     # 
#                                                                                #
#================================================================================#


# Global Variables / Constants that are useful in most scripts
# these can't be set in here, if there are two scripts being called by one another t his will cause problems 
# e.g. sourcing a script inside another sript will die out caus its trying to set 2 constants  and skitz out

# readonly PROGNAME=$(basename $0)
# readonly PROGDIR=$(readlink -m "$(dirname "$0")")
# readonly ARGS="$@"


echo "Figure out cygwin.sh's PATH and TERM shit bro"

#BAR="/cygdrive/c/foo/bar"
#WIN_BAR=$(cygpath -w ${BAR})
#cygpath -w "${BAR}"
#echo "bar: $BAR"
#echo "win_bar: $WIN_BAR"


#add ~bin 2 path

#for i in `echo *.exe | sed 's/\.exe/cc/'`
#do
  #notepad "`cygpath -w $i`"
#done

# detect Cygwin
#cygwin=false;
#case "`uname`" in
  #CYGWIN*) cygwin=true;
#esac
#Then use cygpath as needed:
#
#if $cygwin; then
  #SOMEVAR=`cygpath --path -w $SOMEVAR`
#fi

function cygwin-update-install {
    # TODO Figure out install path dynamically
    pushd /cygdrive/c/cygwin64/
    rm -f ./setup-x86_64.exe
    wget 'https://www.cygwin.com/setup-x86_64.exe'
    chmod 777 ./setup-x86_64.exe
    # todo full path & admin running
    run ./setup-x86_64.exe
    popd

}
