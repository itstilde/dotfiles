#!/usr/bin/env bash
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#                          
#                                                                              # 
#                            Greetings :3                                      #
#         )(  ) )         _                               _  _______  _        #
#        (  ) (          / \                      　    ／   　　　　   ＼　   #
#         )(  )         /   \                    　    /___∠__________＼_|     #
#          ) (         /     \                  　      | |　 ＿ ||  | | 　    # 
#         =====       /       \                         | |∠二ﾆ二||> | |　　　 #　                      
#         |   |      /         \                       /　 _　　 ||  _  \　 　 # 
#   ______|___|_____/           \_______________ 　   /  ( ・）  ||（・) \　   #　 
#  |:::::::::::::::/             \::::::::::::::|　  二土     　c||　　 土二   # 
#  |::::::::::::::/               \:::::::::::::|   二=   ___    ||っ___ = 二  #
#  |:::::::::::::/                 \::::::::::::|    / ﾉ_____／ ξﾉ_____,   \　 #　 　 
#  =============/===================\============ 　/  /　 へ 　へＪ1   \   \  #
#   |#|                                      |#|   /  /　へ　へ｀-'._へ  |  |  #
#   |#|  AUTHOR :  Jacob K.                  |#|   |  ||  へ 　へ  へ　へ＼ |  # 
#   |#|   EMAIL :  jacob@the.nekoba.su       |#|   |  |　　　 　　　　　  | |  # 
#   |#|     WEB :  www.nekoba.su             |#|   |  |　　　 　　　      | |  # 
#   |#| TWITTER :  @killgallic               |#|   |  |　　　 　　　      | |  # 
#   |#|                                      |#|   ヽ_∥ 　   　　　　  　 | |  # 
#   |#|                                      |#|   | |　　　　　　　  　  | |  #
#   |#|                                      |#|   l　ヽ　 　　　　　  　/  /  # 
#   |#| LICENSE :  Private                   |#|    ヽ ＼　 　　　      /  /   #          
#   |#|            Unless otherwise stated   |#|     ＼  ＼＿＿＿＿＿＿／ /    #
#   |#|======================================|#|   　  ＼ ______^_______ /     # 
#                                                                              #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~# 
#
# FILE: global_ps1.sh
#
# PURPOSE: Holds all the stuff for making the command line prompt just that 
# 		   little bit more useful and cute with success smileys
# 
#==============================================================================# 


# shows git branch info / TODO: maybe more info?
__PARSE_GIT_BRANCH() {
    local BRANCH="$(git branch 2> /dev/null | grep "*" | sed "s#*\ \(.*\)#\1#")"
    #[[ "${BRANCH}" != "" ]] && echo "\[${COLOR_RESET}\]\[${COLOR_MAGENTA}\]git\[${COLOR_GRAY}\]:\[${COLOR_RESET}\]\[${COLOR_CYAN}\]${BRANCH} \[${COLOR_RESET}\]"
    #echo "\[${COLOR_CYAN}\]Branch: \[${COLOR_RESET}\]\[${COLOR_CYAN}\]$BRANCH"
    if [[ "${BRANCH}" != "" ]]; then
        # prepend git: for sweet asthetics bros
        PRE="\[${COLOR_RESET}\]\[${COLOR_MAGENTA}\]git\[${COLOR_RESET}\]\[${COLOR_GRAY}\]:" 
        # add colours for specific branches here so kawaii
        MASTER="${PRE}\[${COLOR_RESET}\]\[${COLOR_GREEN}\]${BRANCH}\[${COLOR_RESET}\]"
        REGULAR="${PRE}\[${COLOR_RESET}\]\[${COLOR_PINK}\]${BRANCH}\[${COLOR_RESET}\]"
        # TODO make this a case on an array? or maybe just a loop over an array idk
        [[ "${BRANCH}" == "master" ]] && echo "${MASTER}" || echo ${REGULAR}
     fi
}

# sets prompt to fancy mode 
fancy_ps1(){ 
    export PROMPT_FORMAT="FANCY" 
}
# default boring ps1 without success smileys
boring_ps1(){ 
    export PROMPT_FORMAT="BORING" 
}
# 
__prompt_cmd(){
    
    # preserve and reload history  
    history -a
    history -c
    history -r

    # Change username colour & prompt char on user 
    if [[ ${EUID} == 0 ]] ; then  
        USER_COLOR="\[${COLOR_BRED}\]"
        PROMPT_STYLE="\[${COLOR_WHITE}\]#"
    elif [[ ${USER} == "its" ]]; then
        USER_COLOR="\[${COLOR_ORANGE}\]"
        PROMPT_STYLE="\[${COLOR_GRAY}\]$" 
    else 
        # other users
        USER_COLOR="\[${COLOR_BGREEN}\]"
        PROMPT_STYLE="\[${COLOR_WHITE}\]$" 
    fi 

    # Change color of host if SSH'd somewhere
    if [[ "${SSH_TTY}" ]]; then 
        HOST_COLOR="\[${ASCII_DARK_RED}\]"
    else 
        # local
        HOST_COLOR="\[${ASCII_DARK_BLUE}\]"
    fi

    if [[ "${PROMPT_FORMAT}" == "FANCY" ]]; then
        export PS1="\[${COLOR_RESET}\]${USER_COLOR}\u\[${COLOR_GRAY}\]@${HOST_COLOR}\h \[${COLOR_GREEN}\]\w \[${COLOR_RESET}\]$(__PARSE_GIT_BRANCH)\[${COLOR_RESET}\]\`if [[ \$? == 0 ]]; then echo '\[${COLOR_BGREEN}\]^_^\[${COLOR_RESET}\]'; else echo '\[${COLOR_BRED}\]v_v\[${COLOR_RESET}\]'; fi\`\n\[${COLOR_RESET}\]${PROMPT_STYLE}\[${COLOR_RESET}\] "
    else 
        export PS1="\[${COLOR_RESET}\]${USER_COLOR}\u\[${COLOR_GRAY}\]@${HOST_COLOR}\h \[${COLOR_CYAN}\]\w \[${COLOR_RESET}\]$(__PARSE_GIT_BRANCH)\[${COLOR_RESET}\]\n${PROMPT_STYLE}\[${COLOR_RESET}\] " 
    fi
}

# default prompt
fancy_ps1

export PROMPT_COMMAND=__prompt_cmd
