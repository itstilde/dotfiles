#!/usr/bin/env bash
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#                          
#                                                                              # 
#                            Greetings :3                                      #
#         )(  ) )         _                               _  _______  _        #
#        (  ) (          / \                      　    ／   　　　　   ＼　   #
#         )(  )         /   \                    　    /___∠__________＼_|     #
#          ) (         /     \                  　      | |　 ＿ ||  | | 　    # 
#         =====       /       \                         | |∠二ﾆ二||> | |　　　 #　                      
#         |   |      /         \                       /　 _　　 ||  _  \　 　 # 
#   ______|___|_____/           \_______________ 　   /  ( ・）  ||（・) \　   #　 
#  |:::::::::::::::/             \::::::::::::::|　  二土     　c||　　 土二   # 
#  |::::::::::::::/               \:::::::::::::|   二=   ___    ||っ___ = 二  #
#  |:::::::::::::/                 \::::::::::::|    / ﾉ_____／ ξﾉ_____,   \　 #　 　 
#  =============/===================\============ 　/  /　 へ 　へＪ1   \   \  #
#   |#|                                      |#|   /  /　へ　へ｀-'._へ  |  |  #
#   |#|  AUTHOR :  Jacob K.                  |#|   |  ||  へ 　へ  へ　へ＼ |  # 
#   |#|   EMAIL :  jacob@the.nekoba.su       |#|   |  |　　　 　　　　　  | |  # 
#   |#|     WEB :  www.nekoba.su             |#|   |  |　　　 　　　      | |  # 
#   |#| TWITTER :  @killgallic               |#|   |  |　　　 　　　      | |  # 
#   |#|                                      |#|   ヽ_∥ 　   　　　　  　 | |  # 
#   |#|                                      |#|   | |　　　　　　　  　  | |  #
#   |#|                                      |#|   l　ヽ　 　　　　　  　/  /  # 
#   |#| LICENSE :  Private                   |#|    ヽ ＼　 　　　      /  /   #          
#   |#|            Unless otherwise stated   |#|     ＼  ＼＿＿＿＿＿＿／ /    #
#   |#|======================================|#|   　  ＼ ______^_______ /     # 
#                                                                              #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~# 
#
# This just keeps all my linux functions/shit seperate and out of .bashrc

if which vim > /dev/null 2>&1 ; then 
    export EDITOR=vim
else 
    export EDITOR=vi
fi


PATH="${HOME}/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin"
PATH=$(echo "$PATH" | awk -v RS=':' -v ORS=":" '!a[$1]++{if (NR > 1) printf ORS; printf $a[$1]}') 
export PATH 
# Comes from /etc/profile and is used to munge together paths
pathmunge () {
    case ":${PATH}:" in
        *:"$1":*)
            ;;
        *)
            if [ "$2" = "after" ] ; then
                PATH=$PATH:$1
            else
                PATH=$1:$PATH
            fi
    esac
}

