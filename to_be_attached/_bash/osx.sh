#!/usr/bin/env/bash 

# Force US UTF8 instead of this AU bullshit
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8 
export ARCHFLAGS="-arch x86_64" # architecture flags for brew
export HOMEBREW_CASK_OPTS="--appdir=/Applications"
export HOMEBREW_OPTS="--appdir=/Applications"
export HOMEBREW_GITHUB_API_TOKEN="6b8d0300918f9fbc875b014af9caf77f30f5ce48" 

# default OSX path = '/usr/bin:/bin:/usr/.sbin:/sbin:/usr/local/bin' # start from nuffin 
# This tool uses /etc/paths and /etc/paths.d to build 
#if [ -x /usr/libexec/path_helper ]; then
#eval `/usr/libexec/path_helper -s`
#fi 

PATH="/usr/bin:/bin:/usr/sbin:/sbin";
PATH="/usr/local/sbin:/usr/local/bin:$HOME/bin:$PATH" 
export PATH
# Remove accidental duplicates from path    
# PATH=$(echo "$NEW_PATH" | awk -v RS=':' -v ORS=":" '!a[$1]++{if (NR > 1) printf ORS; printf $a[$1]}')
# http://stackoverflow.com/questions/10193561/tmux-socket-is-not-connected-error-on-os-x-lion sets path properly in tmux
if [ -f "/usr/local/bin/reattach-to-user-namespace" ]; then
/usr/local/bin/reattach-to-user-namespace launchctl setenv PATH $PATH
else
echo 'You need to install reattach-to-user-namespace via brew '
fi 

# set editor - has to be under the path otherwise it uses OSX BSD version of which
#echo $(which -a which)
#which="$(which which)"
#echo "using which $which"

# Add tab completion for many Bash commands
if which brew > /dev/null && [ -f "$(brew --prefix)/etc/bash_completion" ]; then
    source "$(brew --prefix)/etc/bash_completion";
elif [ -f /etc/bash_completion ]; then
    source /etc/bash_completion;
fi;

# Add `killall` tab completion for common apps
complete -o "nospace" -W "Contacts Calendar Dock Finder Mail Safari iTunes SystemUIServer Terminal Twitter Chromium " killall;

# Add tab completion for `defaults read|write NSGlobalDomain` 
complete -W "NSGlobalDomain" defaults;

# osx functions yeeeah
function remote_volume() {
    local user="$1"
    local host="$2"
    local vol="$3"
    ssh "$user"@"$host" 'osascript -e "set volume output volume '"$vol"'"'
}
function osx_ui_reboot() {
    sudo killall -KILL SystemUIServer
    sudo killall -KILL Finder
    sudo killall -KILL Dock 
}
# Opens path finder like finder's open
function pfopen () {
    if [[  $# -eq 0  ]] ; then 
	open -a "Path Finder.app" $PWD
    else 
	if [[ "$@" != "" ]] ; then
	    #use path directly
	    open -a "Path Finder.app" $1
	else
	    open -a "Path Finder.app" $PWD 
	fi
    fi
}

# Man alias
alias man='_() { echo $1; man -M $(brew --prefix)/opt/coreutils/libexec/gnuman $1 1>/dev/null 2>&1;  if [ "$?" -eq 0 ]; then man -M $(brew --prefix)/opt/coreutils/libexec/gnuman $1; else man $1; fi }; _'

# update locate database, linux style
# Figure out what db this is...
alias updatedb='sudo /usr/libexec/locate.updatedb' 

# mdfind is the locate equivalent on OSX
alias locate='mdfind -name' 
