#!/usr/bin/env bash
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#                          
#                                                                              # 
#                            Greetings :3                                      #
#         )(  ) )         _                               _  _______  _        #
#        (  ) (          / \                      　    ／   　　　　   ＼　   #
#         )(  )         /   \                    　    /___∠__________＼_|     #
#          ) (         /     \                  　      | |　 ＿ ||  | | 　    # 
#         =====       /       \                         | |∠二ﾆ二||> | |　　　 #　                      
#         |   |      /         \                       /　 _　　 ||  _  \　 　 # 
#   ______|___|_____/           \_______________ 　   /  ( ・）  ||（・) \　   #　 
#  |:::::::::::::::/             \::::::::::::::|　  二土     　c||　　 土二   # 
#  |::::::::::::::/               \:::::::::::::|   二=   ___    ||っ___ = 二  #
#  |:::::::::::::/                 \::::::::::::|    / ﾉ_____／ ξﾉ_____,   \　 #　 　 
#  =============/===================\============ 　/  /　 へ 　へＪ1   \   \  #
#   |#|                                      |#|   /  /　へ　へ｀-'._へ  |  |  #
#   |#|  AUTHOR :  Jacob K.                  |#|   |  ||  へ 　へ  へ　へ＼ |  # 
#   |#|   EMAIL :  jacob@the.nekoba.su       |#|   |  |　　　 　　　　　  | |  # 
#   |#|     WEB :  www.nekoba.su             |#|   |  |　　　 　　　      | |  # 
#   |#| TWITTER :  @killgallic               |#|   |  |　　　 　　　      | |  # 
#   |#|                                      |#|   ヽ_∥ 　   　　　　  　 | |  # 
#   |#|                                      |#|   | |　　　　　　　  　  | |  #
#   |#|                                      |#|   l　ヽ　 　　　　　  　/  /  # 
#   |#| LICENSE :  Private                   |#|    ヽ ＼　 　　　      /  /   #          
#   |#|            Unless otherwise stated   |#|     ＼  ＼＿＿＿＿＿＿／ /    #
#   |#|======================================|#|   　  ＼ ______^_______ /     # 
#                                                                              #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~# 
#
# misc shit that isn't so much important but fun
#
#==============================================================================# 
# Helper file isn't required here because otherwise when places that are having a problem with it call this to show the fail boat
# it'll cause a recursive can't find my shit problem


# TODO Change case of colour variables to ALL CAPS
# colour table 
list_colour_codes() {
    local fgc bgc vals seq0 esc
    case $OSTYPE in 
        'darwin12'|'darwin13') 
        esc="\033"
        ;;
        *)
        esc="${esc}"
        ;;
    esac


    #printf "Color escapes are %s\n" "${esc}[${value};...;${value}m"
    printf "Values 30..37 are ${esc}[33mforeground colors${esc}[m\n"
    printf "Values 40..47 are ${esc}[43mbackground colors${esc}[m\n"
    printf "Value  1 gives a  ${esc}[1mbold-faced look${esc}[m\n\n"

    # foreground colors
    for fgc in {30..37}; do
        # background colors
        for bgc in {40..47}; do
            fgc=${fgc#37} # white
            bgc=${bgc#40} # black

            vals="${fgc:+$fgc;}${bgc}"
            vals=${vals%%;}

            seq0="${vals:+${esc}[${vals}m}"
            printf "  %-9s" "${seq0:-(default)}"
            printf " ${seq0}TEXT${esc}[m"
            printf " ${esc}[${vals:+${vals+$vals;}}1mBOLD${esc}[m"
        done
        echo; echo
    done

}

# now that's a fail boat.
function failboat() {

    perl -e 'print("#", "=" x 78, "#\n");' 
    perl -e 'print("|", " " x 78, "|\n");' 

    echo -e "${COLOR_BLUE}|${COLOR_WHITE}                           ${COLOR_YELLOW} ${COLOR_YELLOW}(${COLOR_YELLOW}            ${COLOR_YELLOW}(${COLOR_YELLOW}   ${COLOR_YELLOW}(${COLOR_YELLOW}                                ${COLOR_BLUE}|"
    echo -e "${COLOR_BLUE}|${COLOR_WHITE}                           ${COLOR_YELLOW})${COLOR_YELLOW}\ ${COLOR_YELLOW})   ${COLOR_YELLOW}(     ${COLOR_YELLOW})${COLOR_YELLOW}\ )${COLOR_RED})${COLOR_YELLOW}\ ${COLOR_YELLOW})                              ${COLOR_BLUE}|"
    echo -e "${COLOR_BLUE}|${COLOR_WHITE}                           ${COLOR_YELLOW}(${COLOR_RED}(${COLOR_YELLOW})/${COLOR_YELLOW}(   ${COLOR_YELLOW})${COLOR_YELLOW}\   ${COLOR_YELLOW}(${COLOR_RED}(${COLOR_YELLOW})/${COLOR_RED}((${COLOR_YELLOW})/(                             ${COLOR_BLUE}|"
    echo -e "${COLOR_BLUE}|${COLOR_WHITE}                           ${COLOR_RED} /(${COLOR_BRED}_${COLOR_RED})${COLOR_MAGENTA}|${COLOR_RED}((${COLOR_BRED}(_${COLOR_RED})(  /${COLOR_BRED}(${COLOR_RED}_)${COLOR_RED})(${COLOR_BRED}_)${COLOR_RED})                            ${COLOR_BLUE}|"
    echo -e "${COLOR_BLUE}|${COLOR_WHITE}                           ${COLOR_BRED}(_)${COLOR_RED})${COLOR_MAGENTA}_|${COLOR_BRED})\ ${COLOR_MAGENTA}_${COLOR_BRED} ${COLOR_RED})\(${COLOR_MAGENTA}_${COLOR_BRED})${COLOR_RED})(${COLOR_MAGENTA}_${COLOR_BRED})${COLOR_RED})                              ${COLOR_BLUE}|"
    echo -e "${COLOR_BLUE}|${COLOR_WHITE}                           ${COLOR_MAGENTA}|${COLOR_MAGENTA} ${COLOR_MAGENTA}|${COLOR_MAGENTA}_${COLOR_MAGENTA}  ${COLOR_BRED}(_)${COLOR_MAGENTA}_\\\\${COLOR_BRED} (_)${COLOR_MAGENTA}_${COLOR_MAGENTA} ${COLOR_MAGENTA}_${COLOR_MAGENTA}|${COLOR_MAGENTA} |${COLOR_MAGENTA}                              ${COLOR_BLUE}|"
    echo -e "${COLOR_BLUE}|${COLOR_WHITE}                           ${COLOR_MAGENTA}|${COLOR_MAGENTA} ${COLOR_MAGENTA}__|${COLOR_MAGENTA}  ${COLOR_MAGENTA}/${COLOR_MAGENTA} ${COLOR_MAGENTA}_${COLOR_MAGENTA} ${COLOR_MAGENTA}\  ${COLOR_MAGENTA}| ${COLOR_MAGENTA}||${COLOR_MAGENTA} ${COLOR_MAGENTA} |__${COLOR_MAGENTA}                            ${COLOR_BLUE}|"
    echo -e "${COLOR_BLUE}|${COLOR_WHITE}                           ${COLOR_MAGENTA}|${COLOR_MAGENTA}_${COLOR_MAGENTA}|${COLOR_MAGENTA}  ${COLOR_MAGENTA} /${COLOR_MAGENTA}_${COLOR_MAGENTA}/${COLOR_MAGENTA} ${COLOR_MAGENTA}\_${COLOR_MAGENTA}\|${COLOR_MAGENTA}_${COLOR_MAGENTA}_${COLOR_MAGENTA}_${COLOR_MAGENTA}|${COLOR_MAGENTA}_${COLOR_MAGENTA}_${COLOR_MAGENTA}_${COLOR_MAGENTA}_${COLOR_MAGENTA}|${COLOR_MAGENTA} ${COLOR_WHITE}                           ${COLOR_BLUE}|"

    perl -e 'print("|", " " x 78, "|\n");' 
    perl -e 'print("#", "=" x 78, "#\n");' 
    echo -e "${COLOR_BLUE}#                           ${COLOR_MAGENTA}It gone done got broked                            ${COLOR_BLUE}#" 
    perl -e 'print("#", "=" x 78, "#\n");' 
    echo "${COLOR_RESET}"
} 


#NYANNNNN~~~~
function nyan { 
    # colours
    [[ ! $__COLOUR_SCRIPT_SOURCED ]] && source "$HOME/.dotfiles/to_be_attached/_bash/colours.sh"
    printf "${URed}\`·.,¸,.·*\`·.,¸,.·*¯...${BROWN} _______${NC} \n"
    printf "${UYellow}\`·.,¸,.·*¯\`·.,¸,.·*¯..${BROWN}|${NC}${PINK}::;:;;:${NC}${BROWN}|${LIGHTGRAY}/\___/\\ ${NC} \n"
    printf "${UGreen} \`·.,¸,.·*¯\`·.,¸,.·*¯<${BROWN}|${NC}${PINK}:;::;::${NC}${BROWN}|${LIGHTGRAY}(${DARKGRAY}=^ω^=${LIGHTGRAY})'${NC} \n"
    printf "${UPurple}\`-....--""-...--~~'''\`${NC}${DARKGRAY} u${LIGHTGRAY}'${DARKGRAY}u${LIGHTGRAY}'''''''${DARKGRAY}u${LIGHTGRAY}'''''${DARKGRAY}u${NC} \n"
} 

