#!/usr/bin/env bash 

# lints
alias git_phplint="git status -sb | awk '/s/{print $2}' | sed 1d | xargs -n1 php -l"
alias git_ls="git status --porcelain | sed 's/^ M //g' | sed 's/?? //g' |sed 's/^ D //g'"

# composer
alias cu="composer update"
alias ci="composer install"
alias cda="composer dump-autoload -o"

# artisan 
alias art="php artisan"
alias artisan="php artisan"

#youtube
alias youtube-mp3="youtube-dl --extract-audio --audio-format mp3" 

# misc
alias itskey="/bin/cat ~/.ssh/its | pbcopy"
alias itskeypub="/bin/cat ~/.ssh/its.pub | pbcopy"

# vim cron
alias vcron="VIM_CRONTAB=true crontab -e"
#root cron
alias rcron="su -c $(printf "%q " "crontab -e")"

# find out what commands are run the most
alias commandfreq="history | awk '{print $2}' | awk 'BEGIN {FS=\"|\"}{print $1}' | sort | uniq -c | sort -n | tail | sort -nr"

alias hrefxtract=" grep -o '<a href=['\"'\"'\"][^\"'\"'\"']*['\"'\"'\"]' | sed -e 's/^<a href=[\"'\"'\"']//' -e 's/[\"'\"'\"']$//'"

#-------------------
# Personnal Aliases
#-------------------

alias rm='rm -i' # always confirm unless unalias
alias cp='cp -i' 
alias mv='mv -i'
alias less='less -r' # always with the colours

alias mkdir='mkdir -p' # Prevents accidentally clobbering files.
alias ..='cd ..'
alias path='echo -e ${PATH//:/\\n}'
alias libpath='echo -e ${LD_LIBRARY_PATH//:/\\n}'
alias pjet='enscript -h -G -fCourier9 -d $LPDEST' # no idea what this is
    # Pretty-print using enscript

alias du='du -kh'       # Makes a more readable output.
alias df='df -kTh'

#-------------------------------------------------------------
# The 'ls' family (this assumes you use a recent GNU ls)
#-------------------------------------------------------------
#_LS="ls --color=auto"
_LS="ls "
alias ls="${_LS}"
#alias ls="ls -hFG"  # add colors for filetype recognition 
alias lsf="${_LS} --group-directories-first"
alias lss="du -hs $(ls -d ./*) 2>/dev/null | sort -nr" # size of files/folders in dir by size desc
alias lsh="${_LS} -AlG"  # show hidden files
alias lsx="${_LS} -lXB"         # sort by extension
alias lsxl="${_LS} -lSr"         # sort by size, biggest last
alias lstr="${_LS} -ltcr"        # sort by and show change time, most recent last
alias lsar="${_LS} -ltur"        # sort by and show access time, most recent last
alias lsdr="${_LS} -ltr"         # sort by date, most recent last
alias lsless="${_LS} -al | rless"    # pipe through "more"
alias lsr="${_LS} -lR"          # recursive ls
alias lsgraph="ls -R | grep \":$\" | sed -e 's/:$//' -e 's/[^-][^\/]*\//--/g' -e 's/^/   /' -e 's/-/|/'"
if ! type $(which tree) > /dev/null; then
    alias tree="tree -Csu"     # nice alternative to "recursive ls"
fi
#Exit Message

# If your version of 'ls' doesn't support --group-directories-first try this:
# function ll(){ ls -l "$@"| egrep "^d" ; ls -lXB "$@" 2>&-| \
#                egrep -v "^d|total "; }

 alias lsPATH="echo $PATH | tr ':' '\n'"

 alias toptenmem="ps aux | sort -nk +4 | tail"
 alias findduplicates='find -not -empty -type f -printf "%s\n" | sort -rn | uniq -d | xargs -I{} -n1 find -type f -size {}c -print0 | xargs -0 md5sum | sort | uniq -w32 --all-repeated=separate'

 ? () { echo "$*" | bc -l; } # ? 10*3 this is a quick calculator func

alias pyserve="python -m SimpleHTTPServer"
