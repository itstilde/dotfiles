#!/usr/bin/env bash
#
#                           greetings :3                                      #
#        )(  ) )         _                               _  _______  _        #
#       (  ) (          / \                      　    ／   　　　　   ＼　   #
#        )(  )         /   \                    　    /___∠__________＼_|     #
#         ) (         /     \                  　      | |　 ＿ ||  | | 　    # 
#        =====       /       \                         | |∠二ﾆ二||> | |　　　 #
#        |   |      /         \                       /　 _　　 ||  _  \　 　 # 
#  ______|___|_____/           \_______________ 　   /  ( ・）  ||（・) \　   #　 
# |:::::::::::::::/             \::::::::::::::|　  二土     　c||　　 土二   # 
# |::::::::::::::/               \:::::::::::::|   二=   ___    ||っ___ = 二  #
# |:::::::::::::/                 \::::::::::::|    / ﾉ_____／ ξﾉ_____,   \　 #　 　 
# =============/===================\============ 　/  /　 へ 　へｊ1   \   \  #
#  |#|                                      |#|   /  /　へ　へ｀-'._へ  |  |  #
#  |#|  author :  jacob k.                  |#|   |  ||  へ 　へ  へ　へ＼ |  # 
#  |#|   email :  jacob@the.nekoba.su       |#|   |  |　　　 　　　　　  | |  # 
#  |#|     web :  www.nekoba.su             |#|   |  |　　　 　　　      | |  # 
#  |#| twitter :  @killgallic               |#|   |  |　　　 　　　      | |  # 
#  |#|                                      |#|   ヽ_∥ 　   　　　　  　 | |  # 
#  |#|                                      |#|   | |　　　　　　　  　  | |  #
#  |#|                                      |#|   l　ヽ　 　　　　　  　/  /  # 
#  |#| license :  private                   |#|    ヽ ＼　 　　　      /  /   #          
#  |#|            unless otherwise stated   |#|     ＼  ＼＿＿＿＿＿＿／ /    #
#  ============================================   　  ＼ ______^_______ /     #
#                                                                             #   
#=============================================================================#
#                                                                                
# FILE: .helper
#
# PURPOSE: provide functions and mad helpful stuff for other scripts
#	 
#----------------------------------DESCRIPTION---------------------------------#
#  Quick overview 
#   Varialbles
#	verbose : activated using the -v flag
#	loggit : activated using the -l flag 
#
#  Useful Functions list
#
#    box_full_line     : prints  #====# seperator	    	
#
#    box_thin_line     : prints  #----# seperator	    	
#
#    box_empty_line    : prints  #    # gap maker	    	
#
#    box_string	      : prints  text with # in the right places
#
#    box_string_center : prints first argument string w/ dynamic spacing  to
#				ensure # is in the right place and text is centered 
#
#    usage:            : Takes a bash file as an argument and reads that scripts
#				relevant sections out 
#
#    confirmation      : Makes the user hit enter before beginning the process
#				can take an optional string if you want to customize
#
#   TODO
#    make box_string and box_string_centered use this nifty perl one liner
#	 perl -ne ' while( m/.{1,50}/g ){ print "$&\n" }'
#    truncates files at 70 characters
#    Make confirmation overridable for crons
#    Test usage out 
#    check if root user or not 
#    Integrate logging 
#==============================================================================# 


# Box building functions for warnings etc
# Creates an 80col #==# style full line
function box_full_line() { 
    perl -e 'print("#", "=" x 78, "#\n");' 
}

# Creates an 80col #--# style full line 
function box_thin_line() { 
    perl -e 'print("#", "-" x 78, "#\n");' 
}

# Fulls an 80col space with empty space and a # on either end
function box_empty_line() { 
    perl -e 'print("#", " " x 78, "#\n");'
}

function box_string() {
    if [  -z "$1" ] ; then
	return; 
	echo "Parameter missing: function box_string "
    else 
#	if [[ -z "$2" && "$2" == "r" ]] ; then
	    
	
    	msg="$1"
	
	# Check if it has a # comment, if it doesn't throw them in
	# Mostly for when inserting text in the script, not so much docs 
	letter=`echo $msg | cut -c1`
	if [ "$letter" != "#" ] ; then
	    msg="# $msg"
	fi
	
	# eval length $msg was not playing nicely
	slength=`perl -e 'my $len = map $_, "'"$msg"'" =~ /(.)/gs;print "$len"'`
	if [ $slength -gt 75 ] ; then
	      
	     msg=$(echo $msg | perl -ne ' while( m/.{1,70}/g ){ print "$&\n" } ')
	     #echo "Constrained msg: \n $msg" 
	fi
	# Calculate how many spaces we need to throw in
	spaces=$(( 80 - 1 - $slength  )) 

	# Print that line
        perl -e 'print(
	    "'"$msg"'", 
	    " " x '"$spaces"',
	    "#\n" 
	    );' 
    fi 
}
function string_center() {
    if [  -z "$1" ] ; then
	return; 
	echo "Parameter missing: function string_center needs a string to work with"
    else 

	local msg="$@"

	# eval length $msg was not playing nicely, neither was  ${#var}
	# perl to the resque
	
	local slength="${#msg}"

	# If the string we're looking for is even don't pad
	if (( $slength%2 == 0 )); then 
	    msg="$@"
	else 
	   # when the string is an uneven one, it will mean there will need to be another 
	   # space on the right/left side to make the 78 total to keep the box working
	   msg="$@ "
	fi


	# total amount of cols i want is 80, we already have 2 columns as # 
	# this leaves 78 columns to play with
	# TODO break up strings bigger than 78 chars
	# so if the string was 10 cars, we have 68 remaining spaces to distribute  
	# on each side of the string. 

	slength="${#msg}" 
	local spaces_total=$(( 78 - $slength ))
	#echo "total spaces remaining to distribute EVEN? $spaces_total"
	local spaces=$(( $spaces_total / 2  ))
	#echo "spaces halved $spaces"


	perl -e "print(' ', ' 'x${spaces}, '${msg}', ' 'x${spaces}, ' ');" 
	perl -e  'print("\n");'

   
    fi 
    #get_stack
    #echo $STACK
}
function box_string_center() {
    if [  -z "$1" ] ; then
	return; 
	echo "Parameter missing: function box_string_center needs a string to work with"
    else 

	local msg="$@"

	# eval length $msg was not playing nicely, neither was  ${#var}
	# perl to the resque
	
	local slength="${#msg}"

	# If the string we're looking for is even don't pad
	if (( $slength%2 == 0 )); then 
	    msg="$@"
	else 
	   # when the string is an uneven one, it will mean there will need to be another 
	   # space on the right/left side to make the 78 total to keep the box working
	   msg="$@ "
	fi


	# total amount of cols i want is 80, we already have 2 columns as # 
	# this leaves 78 columns to play with
	# TODO break up strings bigger than 78 chars
	# so if the string was 10 cars, we have 68 remaining spaces to distribute  
	# on each side of the string. 

	slength="${#msg}" 
	local spaces_total=$(( 78 - $slength ))
	#echo "total spaces remaining to distribute EVEN? $spaces_total"
	local spaces=$(( $spaces_total / 2  ))
	#echo "spaces halved $spaces"


	perl -e "print('#', ' 'x${spaces}, '${msg}', ' 'x${spaces}, '#');" 
	perl -e  'print("\n");'

   
    fi 
    #get_stack
    #echo $STACK
}


# FUNCTION usage
#  Reads and echo's contents between 2 #=---*'s at the start of a file
#  at the top of yo file NIGGAH
function helper_help() {
    
    full_file_path="$1"		 # basic strings of the file info so we can access it 
    folder=${1%/*} 
    filename="$(basename "$1")" 
    header_lines=29	 	 # amount of space the pretty header takes up
    i=0;	 		 # incrementer to make sure we don't print out useless header info 
    filename_length=${#filename} #length of the file name
    file="$folder/$filename";    # BUG: For some reason $full_file_path didn't work?
 
    # How many characters we have left to fill with spaces
    # (total length of our box -  length of string  - length of file name) / 2
    remaining_spaces=$(( (80 - 21 - $filename_length)/2 )) # 78 caus of the |'s 
    
    # Make a box and put the header in 
    # perl's inbuilt print lets us do it with numbers, instead of having to loop in bash 
    box_thin_line 
    box_empty_line 
    perl -e 'print( "#", 
	" " x '$remaining_spaces',  
	"Usage and Info for: '"$filename"'" , 
	" " x  '$remaining_spaces', 
	"#\n"
    )'	
    box_empty_line
    box_thin_line 

    # start reading the file to grab its documentation
    cat $file | while read line
    do
	# Start at 0 and don't do anything with the header
	let i++ 
	(( $i < $header_lines )) && continue; 
 
	# Jump over the first #--- line, caus its the heeeader
	#Break out of the loop since we're done hurr 
 	[[ $line =~ ^#=========.* ]] && break;
	[[ $line =~ ^#-.*DESCRIPTION ]] && continue; 
	line_length=${#line} #length of the file name 

		
	spaces_left=$(( ( 80 - 1 - $line_length) )) # Amount of spaces there left to make up 80 

	perl -e 'print("'"$line"'", " " x '$spaces_left', "#\n" )'
    done 
    box_thin_line
}

function func_exists() {
    declare -f -F $1 > /dev/null
	return $?
}


# Starting message :3
function helper_init() {
    
    # swap verbose
    #[[ $verbose -eq 1 ]] && startup_msg="$startup_msg | Verbose Mode On"
    #[[ $loggit -eq  1 ]] &&  startup_msg="$startup_msg | Logging Mode on"

    box_thin_line 
    box_string_center "| Helper Active | "
    box_thin_line
} 

func_exists yesnoquit || source "${HOME}/.bash/global_functions.sh"
