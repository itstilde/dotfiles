#!/usr/bin/env bash
# define colors using tput or an ASCII fallback
# More colour stuff here: http://tldp.org/LDP/abs/html/colorizing.html

if tput setaf 1 &> /dev/null; then
    # TPUT 
    # http://tldp.org/HOWTO/Bash-Prompt-HOWTO/x405.html

    # TODO - check for other scripts still using COLOR_LIGHTRED etc
    COLOR_RED=$(tput sgr0 && tput setaf 1) 
    COLOR_GREEN=$(tput sgr0 && tput setaf 2)

    COLOR_ORANGE=$(tput sgr0 && tput setaf 3) # actually yellow
    COLOR_YELLOW=$(tput sgr0 && tput setaf 3 && tput bold) # actually bold orange

    COLOR_BLUE=$(tput sgr0 && tput setaf 4) # actually magenta
    COLOR_MAGENTA=$(tput sgr0 && tput setaf 4 && tput bold) # actually bold blue

    COLOR_PINK=$(tput sgr0 && tput setaf 5 ) 
    COLOR_CYAN=$(tput sgr0 && tput setaf 6 )

    COLOR_GRAY=$(tput sgr0 && tput setaf 7)
    COLOR_WHITE=$(tput sgr0 && tput setaf 7 && tput bold) 

    COLOR_BRED=$(tput sgr0 && tput setaf 1 && tput bold) 
    COLOR_BGREEN=$(tput sgr0 && tput setaf 2 && tput bold) 
    COLOR_BPINK=$(tput sgr0 && tput setaf 5 && tput bold) 
    COLOR_BCYAN=$(tput sgr0 && tput setaf 6 && tput bold)

    COLOR_URED=$(tput sgr1 && tput sgr 0 1 && tput setaf 1)
    COLOR_UBLUE=$(tput sgr0 && tput setaf 4)


    ASCII_BLUE="\033[0;34m"
    ASCII_DARK_BLUE="\033[1;34m"
    ASCII_RED="\033[0;31m\]"
    ASCII_DARK_RED="\033[1;31m" 

    COLOR_RESET=$(tput sgr0) 

else

    # OSX has a different escape code!
    case $OSTYPE in 
        'darwin12'|'darwin13') 
            ESCC="\033"
        ;;
        *)
            ESCC="\e"
        ;;
    esac

    ASCII_BLACK="$ESCC[0;30m"    # Black / Regular
    ASCII_RED="$ESCC[0;31m"      # Red
    ASCII_GREEN="$ESCC[0;32m"    # Green
    ASCII_YELLOW="$ESCC[0;33m"   # Yellow
    ASCII_BLUE="$ESCC[0;34m"     # Blue
    ASCII_PURPLE="$ESCC[0;35m"   # Purple
    ASCII_CYAN="$ESCC[0;36m"     # Cyan
    ASCII_WHITE="$ESCC[0;37m"    # White

    ASCII_BBLACK="$ESCC[1;30m"   # BBlack / Bold
    ASCII_BRED="$ESCC[1;31m"     # BRed
    ASCII_BGREEN="$ESCC[1;32m"   # BGreen
    ASCII_BYELLOW="$ESCC[1;33m"  # BYellow
    ASCII_BBLUE="$ESCC[1;34m"    # BBlue
    ASCII_BPURPLE="$ESCC[1;35m"  # BPurple
    ASCII_BCYAN="$ESCC[1;36m"    # BCyan
    ASCII_BWHITE="$ESCC[1;37m"   # BWhite
    
    ASCII_UBLACK="$ESCC[4;30m"   # UBlack / Underline
    ASCII_URED="$ESCC[4;31m"     # URed
    ASCII_UGREEN="$ESCC[4;32m"   # UGreen
    ASCII_UYELLOW="$ESCC[4;33m"  # UYellow
    ASCII_UBLUE="$ESCC[4;34m"    # UBlue
    ASCII_UPURPLE="$ESCC[4;35m"  # UPurple
    ASCII_UCYAN="$ESCC[4;36m"    # UCyan
    ASCII_UWHITE="$ESCC[4;37m"   # UWhite
    
    ASCII_BGBLACK="$ESCC[40m"    # BGBlack - background
    ASCII_BGRED="$ESCC[41m"      # BGRed
    ASCII_BGGEEEN="$ESCC[42m"    # BGGreen
    ASCII_BGYELLOW="$ESCC[43m"   # BGYellow
    ASCII_BGBLUE="$ESCC[44m"     # BGBlue
    ASCII_BGPURPLE="$ESCC[45m"   # BGPurple
    ASCII_BGCYAN="$ESCC[46m"     # BGCyan
    ASCII_BGWHITE="$ESCC[47m"    # BGWhite
    ASCII_BROWN="$ESCC[0;33m"
    ASCII_LIGHTGRAY="$ESCC[0;37m"
    ASCII_DARKGRAY="$ESCC[1;30m"
    ASCII_PINK="$ESCC[35;40m" 
    ASCII_NC="$ESCC[0m"          # No color	
fi
